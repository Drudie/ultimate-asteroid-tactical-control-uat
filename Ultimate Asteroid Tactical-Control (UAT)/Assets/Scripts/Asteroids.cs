﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroids : MonoBehaviour
{
    private Vector3 playerPosition;
    private Vector3 movement;

    // Start is called before the first frame update
    void Start()
    {
        playerPosition = GameManager.gm.player.transform.position;
        movement = playerPosition - transform.position;
        movement.Normalize();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(movement * GameManager.gm.asteroidSpeed * Time.deltaTime);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (!GameManager.gm.doRemoveEnemies)
        {
            if (other.gameObject.tag == "Background")
            {
                GameManager.gm.activeEnemies.Remove(this.gameObject);
                Destroy(this.gameObject);
            }
        }
    }
}
