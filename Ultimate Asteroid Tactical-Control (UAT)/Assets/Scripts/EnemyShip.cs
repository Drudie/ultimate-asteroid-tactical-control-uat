﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShip : MonoBehaviour
{
    private Vector3 playerPosition;
    private Vector3 movement;

    // Start is called before the first frame update
    void Start()
    {
        playerPosition = GameManager.gm.player.transform.position;
        movement = playerPosition - transform.position;
        movement.Normalize();
    }

    // Update is called once per frame
    void Update()
    {
        //Have the enemy ship track the player by setting its right vector to the players current position
        transform.right = GameManager.gm.player.transform.position - transform.position;
        transform.position += (transform.right * GameManager.gm.enemyShipSpeed * Time.deltaTime);
    }
}
