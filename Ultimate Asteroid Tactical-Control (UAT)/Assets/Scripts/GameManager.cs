﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager gm = null;

    //Enemy spawn points
    public GameObject[] spawnPoints;

    //Enemy list
    public List<GameObject> enemies;

    //Enemy variables
    public List<GameObject> activeEnemies;
    public int enemyMax;
    public bool doRemoveEnemies;
    public int asteroidSpeed;
    public int enemyShipSpeed;

    //Bullet variables
    public GameObject bullet;
    public int bulletSpeed;
    public int bulletLifeTime;

    //Player variables
    public GameObject player;

    //Score and lives + their display texts
    public int score;
    public Text scoreText;
    public int lives;
    public Text livesDisplay;

    // Start is called before the first frame update
    void Awake()
    {
        gm = this;
        if (gm == null)
        {
            gm = this; // Store THIS instance of the class (component) in the instance variable
            DontDestroyOnLoad(gameObject); // Keep the game manager on load of a new scene
        }
        else if(gm != this)
        {
            Destroy(this.gameObject); // We only want one game manager, so if a 2nd is created, delete it.
        }
    }

    // Start is called before the first frame update
    private void Start()
    {
        activeEnemies = new List<GameObject>();
        doRemoveEnemies = false;
        SetScore();
    }

    // Update is called once per frame
    private void Update()
    {
        EnemyAdd();
        SetScore();
        SetLives();
    }

    void EnemyAdd()
    {
        if (activeEnemies.Count < enemyMax)
        {
            //Choose from spawn point list 
            int spawnNumber = Random.Range(0, spawnPoints.Length);
            GameObject spawn = spawnPoints[spawnNumber];

            //Choose enemy to spawn
            GameObject enemy = enemies[Random.Range(0, enemies.Count)];

            //Create the enemy
            GameObject enemyInstance = Instantiate<GameObject>(enemy, spawn.transform.position, Quaternion.identity);

            //Add to the active list
            activeEnemies.Add(enemyInstance);
        }
    }
    
    public void RemoveEnemies()
    {
        doRemoveEnemies = true;
        for(int i = 0; i < activeEnemies.Count; i++)
        {
            Destroy(activeEnemies[i]);
        }
        activeEnemies.Clear();
        doRemoveEnemies = false;
    }

    public void LoseLife()
    {
        lives--;
        if(lives == 0)
        {
            //Send the player back to the menu after losing all lives
            SceneManager.LoadScene("MainMenu");
        }
        else
        {
            //Remove all enemies and spawn the player back at the origin
            RemoveEnemies();
            player.transform.position = new Vector3(0, 0, 0);
        }
    }

    void SetScore()
    {
        scoreText.text = "SCORE: " + score.ToString();
    }
    
    void SetLives()
    {
        livesDisplay.text = "LIVES: " + lives.ToString();
    }
}
