﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Vector3 direction;
    // Start is called before the first frame update
    void Start()
    {
        direction = GameManager.gm.player.transform.right;
        Destroy(this.gameObject, GameManager.gm.bulletLifeTime);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(direction * Time.deltaTime * GameManager.gm.bulletSpeed);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Asteroid")
        {
            GameManager.gm.activeEnemies.Remove(other.gameObject);
            Destroy(this.gameObject);

            GameManager.gm.score += 100;

            Destroy(other.gameObject);
        }
        if(other.gameObject.tag == "EnemyShip")
        {
            GameManager.gm.activeEnemies.Remove(other.gameObject);
            Destroy(this.gameObject);

            GameManager.gm.score += 100;

            Destroy(other.gameObject);
        }
    }
}
