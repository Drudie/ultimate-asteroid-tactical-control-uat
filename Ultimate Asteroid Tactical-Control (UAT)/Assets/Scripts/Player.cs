﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Transform tf;
    //Movement speed for designer to change
    public float movementSpeed;

    //Rotation speed for designer to change
    public float rotationSpeed;


    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        //Ship movement/rotation
        //W/S for moving forward/backward, A/D for rotating left/right
        if (Input.GetKey(KeyCode.W))
        {
            tf.position += (tf.right * movementSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.A))
        {
            tf.Rotate(new Vector3(0, 0, Time.deltaTime * rotationSpeed));
        }
        if (Input.GetKey(KeyCode.S))
        {
            tf.position -= (tf.right * movementSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            tf.Rotate(new Vector3(0, 0, Time.deltaTime * -rotationSpeed));
        }

        //Bullet creation on press
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //Create the bullet
            GameObject bullet = Instantiate<GameObject>(GameManager.gm.bullet);
            //Have the bullet start from the ships current position
            bullet.transform.position = this.transform.position;
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Asteroid" | other.gameObject.tag == "EnemyShip")
        {
            GameManager.gm.LoseLife();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Background")
        {
            GameManager.gm.LoseLife();
        }
    }
}

